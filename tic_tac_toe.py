import sys
import os
import random

import pygame
from pygame.sprite import Sprite

from menu_button import MenuButton
from box import Box
from settings import Settings
from players import Player

class TicTacToe:
    def __init__(self):

        pygame.display.init()
        pygame.font.init()
        self.settings = Settings()
        self.players = Player()

        self.screen = pygame.display.set_mode((self.settings.screen_width, self.settings.screen_height))
        self.screen_rect = self.screen.get_rect()
        self.game_width = self.screen_rect.width
        self.game_height = self.screen_rect.height

        self.game_running = True
        self.game_active = False

        #boxy
        self.boxes = pygame.sprite.Group() #grupa na boxy 
        
        self.play_button1 = MenuButton(self, "Graj!", 0)
        self.play_button2 = MenuButton(self, "Graj przeciwko AI", 60)

        self._draw_boxes()
        self._player_text()

    def _draw_boxes(self):
        col = 0
        for row in range(0,9):
            if row%3 == 0:
                col += 1
            box = Box(self)
            box.number = row
            box.rect.x = 20 + box.width * (row%3) * 1.02
            box.rect.y = 25 + 1.02 * col * box.height - box.height
            self.boxes.add(box)

    def _who_start_first(self):
        rand_num = random.randint(0,1)
        if rand_num == 1:
            self.players.computer_turn = True
        else:
            self.players.computer_turn = False

    def _box_click(self, mouse_pos):
        player = 0
        if self.settings.computer_game and self.players.computer_turn:
            self._AI_move()
        elif self.settings.computer_game and not self.players.computer_turn:
            for box in self.boxes:
                self.box_clicked = box.rect.collidepoint(mouse_pos)
                if self.box_clicked == 1 and box.box_color == box.box_color_s:
                    box.box_color = (52, 73, 235)
                    box.status = 1
                    player = 1
                    self.players.player_text = "Komputer"
                    self.players.computer_turn = True
                    self._check_winner(player)
        elif not self.settings.computer_game:
            for box in self.boxes:
                self.box_clicked = box.rect.collidepoint(mouse_pos)
                if self.box_clicked == 1 and box.box_color == box.box_color_s:
                    if self.players.player == 1:
                        box.box_color = (52, 73, 235)
                        box.status = 1
                        player = 1
                    else:
                        box.box_color = (20, 179, 30)
                        box.status = 2
                        player = 2
                    self._change_player()
                    self._check_winner(player)
    
    def _check_play_or_custom_button(self, mouse_pos):
        self.button_play_clicked = self.play_button1.rect.collidepoint(mouse_pos)
        self.button_custom_clicked = self.play_button2.rect.collidepoint(mouse_pos)
        if self.button_play_clicked:
            self.game_active = True
        elif self.button_custom_clicked:
            self.game_active = True
            self.settings.computer_game = True
            self._who_start_first()

    def _player_text(self):
        self.msg_image = self.settings.font.render(self.players.player_text, True, self.settings.text_color, self.settings.bg_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.screen_rect.center
        self.msg_image_rect.top = self.screen_rect.top
        self.screen.blit(self.msg_image, self.msg_image_rect)

    def _change_player(self):
        if self.players.player == 1:
            self.players.player = 2
            self.players.player_text = "Gracz: 2"
        else:
            self.players.player = 1
            self.players.player_text = "Gracz: 1"

    def _get_results(self):
        results = []
        for box in self.boxes:
            results.append(box.status)
        return results

    def _winner_message(self, player, g_type = 1):
        if g_type == 1:
            self.players.player_text = f"Wygrywa gracz: {player}"
        else:
            self.players.player_text = f"Remis! "
        self.players.computer_turn = False
        self.game_running = False

    def _check_winner(self, player):
        results = self._get_results()
        if  (results[0] == player and results[1] == player and results[2] == player) or \
            (results[3] == player and results[4] == player and results[5] == player) or \
            (results[6] == player and results[7] == player and results[8] == player):
            self._winner_message(player)
        elif(results[0] == player and results[3] == player and results[6] == player) or \
            (results[1] == player and results[4] == player and results[7] == player) or \
            (results[2] == player and results[5] == player and results[8] == player):
            self._winner_message(player)
        elif(results[0] == player and results[4] == player and results[8] == player) or \
            (results[2] == player and results[4] == player and results[6] == player):
            self._winner_message(player)
        elif(results[0] != 0 and results[1] != 0 and results[2] != 0) and \
            (results[3] != 0 and results[4] != 0 and results[5] != 0) and \
            (results[6] != 0 and results[7] != 0 and results[8] != 0):
            self._winner_message(player, 0)


    def _AI_move(self):
        rand_num = random.randint(0,9)
        for box in self.boxes:
            if box.status == 0 and box.box_color == box.box_color_s and rand_num == box.number:
                box.box_color = (179, 44, 20)
                box.status = 'Komputer'
                player = 'Komputer'
                self.players.player_text = "Gracz 1"
                self.players.computer_turn = False
                self._check_winner(player)

    def _get_events(self):
        e = pygame.event.poll()
        if e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE:
            sys.exit()
        elif e.type == pygame.MOUSEBUTTONDOWN and e.button == 1:
            mouse_pos = pygame.mouse.get_pos()
            if not self.game_active:
                self.boxes.empty()
                self._draw_boxes()
                self._check_play_or_custom_button(mouse_pos)
            else:
                self._box_click(mouse_pos)

    def _reset(self):
        pygame.display.update()
        self.game_active = False
        self.game_running = True
        self.settings.computer_game = False
        self.run_game()

    def update_screen(self):
        self.screen.fill(self.settings.bg_color)
        for box in self.boxes:
            box.screen.fill(box.box_color, box.rect)
        if not self.game_active:
            self.play_button1.draw_button()
            self.play_button2.draw_button()
        self._player_text()
        pygame.display.update()

    def run_game(self):
        self.update_screen()
        while self.game_running:
            self._get_events()
            if self.game_active:
                self.update_screen()
            if self.settings.computer_game and self.players.computer_turn:
                self._AI_move()
        self._reset()

if __name__ == "__main__":
    t = TicTacToe()
    t.run_game()
import pygame
from pygame.sprite import Sprite
from settings import Settings

class Box(Sprite):
    def __init__(self, t_game):
        super().__init__()
        self.screen = t_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = Settings()

        #ustawienia boxów
        self.width, self.height = self.settings.box_width, self.settings.box_height
        self.box_color = (0,0,0)
        self.box_color_s = (0,0,0)

        #gracz
        self.status = 0

        #rysowanie boxa
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.x = 0
        self.rect.y = 0


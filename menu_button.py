import pygame
from pygame.sprite import Sprite
from settings import Settings

class MenuButton(Sprite):
    def __init__(self, t_game, msg, pos):
        super().__init__()
        self.screen = t_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = Settings()

        self.width, self.height = 200, 50
        self.button_color = (207, 184, 12)
        self.text_color = (30, 30, 30)

        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.x = self.screen_rect.centerx - 100
        self.rect.y = self.screen_rect.centery - 100 + pos

        self.prep_msg(msg)

    def prep_msg(self, msg):
        self.msg_image = self.settings.font.render(msg, True, self.text_color, self.button_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_button(self):
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)
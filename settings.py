import os
import sys
import pygame

class Settings:
    def __init__(self):
        #screen
        self.screen_width = 600
        self.screen_height = 600
        self.bg_color = (100, 100, 100)

        #window
        self.caption = "TicTacToe"
        self.icon = 'icon.ico'
        self.set_caption(self.caption)
        self.set_icon(self.icon)

        #boxes
        self.box_width = 185
        self.box_height = 185

        #computer player
        self.computer_game = False

        #font
        self.text_color = (0, 0, 0)
        self.font = pygame.font.SysFont("comicsansms", 16)


    def set_caption(self, caption):
        pygame.display.set_caption(caption)

    def set_icon(self, icon):
        if os.path.exists(icon) and os.path.isfile(icon):
            icon = pygame.image.load(os.path.join('.', icon))
            pygame.display.set_icon(icon)